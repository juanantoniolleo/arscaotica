# Ars Caótica - Juan Antonio Lleó

Ars Caótica

"Ars Caótica: Diálogos entre Arte, Ciencia y Tecnología"

RESUMEN:
"Si buscas resultados distintos, no hagas siempre lo mismo." Albert Einstein

Planteado como un diálogo entre el ponente y los participantes, el objetivo es tratar temas tales como la creación artística, tanto sonora como audiovisual y de qué forma se puede buscar inspiración y estrategias creativas en disciplinas tales como las ciencias y la tecnología, principalmente matemáticas, física, informática y electrónica que, aunque aparentemente lo parezcan, no son tan ajenas o tan diferentes a la creación artística, en lo que respecta a la necesidad de abstraerse para conseguir ampliar la visión y obtener nuevos enfoques que conduzcan a soluciones diferentes.
Se analizarán distintas estrategias creativas que pueden servir, tanto para afrontar proyectos artísticos que tengan un sello personal, como para entender mejor las obras de creadores que se nutren del campo denominado, Arte Ciencia y Tecnología (ACT).

REPOSITORIOS:
https://gitlab.com/juanantoniolleo/arscaotica

GOOGLE DRIVE:
https://drive.google.com/drive/folders/1OEGfr4tqWXNW4PJNkQvzr4ICfmbJnWKN?usp=sharing




