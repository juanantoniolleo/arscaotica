## Tsoundnami
### Tsoundnami – Juan Antonio Lleó (2011)

El fascinante sonido de una pequeña campana japonesa, que me fue regalada
como souvenir hace ya bastantes años, aporta el único sonido que es la base
de todos los demás que aparecen en la obra.
Tras sucesivas reelaboraciones electroacústicas, principalmente mediante
síntesis granular y resíntesis espectral, se van generando los distintos
materiales sonoros. El título hace referencia a una especie de metáfora: tratar
en algunos momentos de evocar las sensaciones que me produjo, durante una
corta estancia en Japón, conocer sus distintos paisajes y un poco de su cultura,
y recrear la experiencia de inmersión en una suerte de tsunami sonoro. ¡Ójala
todos ellos fueran así de inofensivos!
Esta pieza quiere ser un pequeño homenaje al pueblo japonés y está dedicada
a la memoria de los desaparecidos en el Tsunami de Japón en 2011.