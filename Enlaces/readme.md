## ENLACES:

Enlaces a cosas que hago o en proyectos en los que he participado, algunos están repetidos.

Espero que encuentres algo interesante y por favor ¡a pequeñas dosis!.

Muchas gracias por tu interés,

Juan Antonio Lleó

### Conferencias:
http://doc.lleo.net/Dominando_el_azar-Juan_Antonio_Lleo.pdf
http://doc.lleo.net/Ordenando_la_Incertidumbre-J_A_Lleo.pdf

Capitulo de libro:
http://doc.lleo.net/ARTE_INTERACTIVO-El_arte_en_las_Redes-J_A_Lleo.pdf

### NANO Y NANOARTE:

http://doc.lleo.net/Nanoosfera_Seleccion_Paris.pdf

PREMIO CIENTÍFICO SPMAGE, COLABORÉ COMO COMISARIO EN VARIAS EXPOSICIONES Y COMO JURADO EN SU SEGUNDA EDICIÓN:

http://doc.lleo.net/Catalogo_SPMAGE07.pdf

http://doc.lleo.net/SPMAGE_09_Premios.pdf

También tenía obras en Artstack, pero creo que ya no funciona: ;-)

http://theartstack.com/artists/juan-antonio-lleo

------------------------------------------------------------------------------------------------------------------------------

### LINKS & COMMENTS:

Algunos vídeos e información sobre mi instalación MIDIVERSO, que presenté en el primer SONAR y que por cierto, le encanta a los niños:

http://lared.lleo.net

Es la copia de una web antigua, el diseño es mejorable...

Contiene algunos vídeos e información sobre mi instalación MIDIVERSO, que presenté en el primer SONAR y que por cierto, le encanta a los niños.

Enlaces a videos de Midiverso más moderno:

http://media.lleo.net/Midiverso_Convergencias.mp4


Exposición Convergencias, otro proyecto, Código Especular:

http://media.lleo.net/Codigo_Especular_Convergencias.mp4



*******************************************************************************


### ENLACES DE MÚSICA Y VIDEOS AL SERVIDOR, PARA DESCARGAR:


Juan Antonio Lleó - Selection of Electroacoustic Compositions:

2014:

Versión estéreo de la obra FOOC-PULSENOS, del concierto en el Auditorio 400 del Reina Sofia, en la que me he basado y otras casi 100 composiciones de otros artistas:


http://media.lleo.net/Juan_A_Lleo-Foucault_Out_Of_Cage.wav

FOOC - Pulsenos (micro site del proyecto):

http://fooc.lleo.net/

AUDIO MAD, 100 ARTISTAS SONOROS DE MADRID: Esta obra está editado en el DVD Audio Mad, junto con otras casi 100 composiciones de otros artistas:

http://audio-mad.bandcamp.com/releases

https://audiopoliscentrocentro.wordpress.com/audiopolis/artistas/audio-mad/

2012:

Dos piezas para flauta y electroacústica, desarrolladas con la flautista de pico Anna Margules y su alumna, Sandra Sáiz. Estrenadas respectivamente en Madrid y Lugo. Los ficheros contienen sólo la banda electroacústica, no existe grabación aún de la obra completa.

http://media.lleo.net/Orange_Noise-J_A_Lleo.mp3

http://media.lleo.net/Granular_Nature-J_A_Lleo.mp3

2011:

http://media.lleo.net/TSOUNDNAMI-J_A_Lleo.mp3

2010:

BELL0, BELL1. Concierto para campanas y electroacústica, con Llorenç Barber en el Festival Anem Anem, Gandía, 2010. Los ficheros contienen sólo la banda electroacústica, elaborada a partirt de material grabado junto con Llorenç en el LIEM. No existe grabación aún de la obra completa.

http://media.lleo.net/5Toques_Dobles-Bell0_Bell1-J_A_Lleo-L_Barber.mp3

http://media.lleo.net/Arpegios-Bell0_Bell1-J_A_Lleo-L_Barber.mp3

http://media.lleo.net/Caja_Musica-Bell0_Bell1-J_A_Lleo-L_Barber.mp3

http://media.lleo.net/Gamelan_Spear-Bell0_Bell1-J_A_Lleo-L_Barber.mp3

http://media.lleo.net/Repiques-Bell0_Bell1-J_A_Lleo-L_Barber.mp3

http://media.lleo.net/Seculenta-Bell0_Bell1-J_A_Lleo-L_Barber.mp3

http://media.lleo.net/Toque_Multifon-Bell0_Bell1-J_A_Lleo-L_Barber.mp3

http://media.lleo.net/Toques-Bell0_Bell1-J_A_Lleo-L_Barber.mp3

2008:

Composición electroacústica para el CD incluido en la colección de 63 grabados “El juego de la Oca”, editado a beneficio de la Fundación Vicente Ferrer, por Pablo Rodríguez Guy, Madrid.

http://media.lleo.net/Dados_2_Dados-J_A_Lleo.mp3


2006:

Autorretrato (El gran Golpe): Composición electroacústica dentro de la exposición “Selfportrait - A show for Bethlem”, comisariada por Wilfried Agricola de Cologne, en la Al Kahf Art Gallery, en el Bethlehem International Center en Belén (Palestina). La exposición se presentó también en: Offizyna Art Space in Szczecin, Polonia; Casoria Contemporary Art Museum, Nápoles, Italia; Museo de Arte Contemporáneo (MAC) de Santa Fe y Museo de Arte Contemporáneo de Rosario (MACRO), ambos en Argentina.

http://media.lleo.net/El_Gran_Golpe_-_J.A.Lleo.mp3

2004:

Weyujoi 5: Obra electroacústica. Estrenada en el WORK OVERTURE PROJECT, 34rd FESTIVAL SYNTHESE BOURGES, Francia. Publicado en el CD RAS nº 7 (Revista de Arte Sonoro) Facultad de Bellas Artes de Cuenca, Universidad de Castilla la Mancha (2005).

http://media.lleo.net/Weyujoi-Lleo.mp3


Midiverso: instalación interactiva, grabación realizada en el Festival Confluencias.

http://media.lleo.net/Midiverso_Confluencias.mp3


2000:

EXPO 2000: Composición electroacústica, en colaboración con Francisco Felipe, para el Pabellón de la ONU, EXPO 2000 Hannover:

http://media.lleo.net/Expo_2000_-_Medio_Ambiente.mp3

http://media.lleo.net/Expo_2000_-_Humanidad.mp3


1988:

“Límites, cuando FB tiende a infinito”: Composición midi, para pequeño sintetizador digitalFM Yamaha FB01:

http://media.lleo.net/Limites_2_Orquestal.mp3

http://media.lleo.net/Limites_1_Crescendos.mp3


### Juan Antonio Lleó – Video List/Listado de videos:


### Juan Antonio Lleó (Music & Visuals):

http://media.lleo.net/OUR_Lleo.mp4


### Juan Antonio Lleó (Music & Visuals) and Domingo Sarrey (Visuals):

http://media.lleo.net/DBC_Lleo-Sarrey.mp4

http://media.lleo.net/Mylar_Estudio_Lleo-Sarrey.mp4


### Juan Antonio Lleó (Music & Visuals), Juan Carlos Carrazón and Javier Bedrina

(Visuals):

http://media.lleo.net/UversoVCD.mov


### Colaboraciones con Antonio Alvarado:


http://www.antonioalvarado.net/videos/2003_torre_espejos/torre_espejos_v.htm



### DOCUMENTOS Y CATALOGOS:


Exposición El Viaje de e-ro.es: http://doc.lleo.net/Viaje_Eroes.pdf


CONVERGENCIAS - CAB BURGOS:


http://doc.lleo.net/CatalogoConvergencias.pdf


ARTE POSTFRACTAL:


http://doc.lleo.net/Arte_Postfractal_J.A.Lleo_Catalogo.pdf


ART OF THE DIGITAL AGE:


http://doc.lleo.net/Art_Digital_Ages.pdf

http://doc.lleo.net/Art_Digital_Ages_p60.pdf


MAEM:


http://doc.lleo.net/CatalogoMAEM07-Comisario_J_A_Lleo.pdf


NANO:


http://doc.lleo.net/Nanoosfera_Seleccion_Paris.pdf


http://doc.lleo.net/Catalogo_SPMAGE07.pdf


http://doc.lleo.net/SPMAGE_09_Premios.pdf


